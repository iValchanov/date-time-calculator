# Date Time Calculator

## Description
A program that performs various calculations with date and time values. 

## Roadmap
##### Task #1:
1.1 Read four values from the standard input - separator symbol, date, month and year\
1.2 Print the values on the standard output as follows: 
* The separator symbol separates the other three values
* The date and month are two characters wide 

Example input: / 1 2 2023 \
Example output: 01/02/2023


Example input: . 1 2 2023 \
Example output: 01.02.2023

1.3 Add the `printf` statement below to your `main` function:
* `printf("the date is even number: %d", <expression>);`
* Write an expression containing **bitwise** operator  to check if the given date is an even number (the expression can consist of multiple opearators)
* Write another expression **without bitwise** operator that performs the same check

Example input: / 1 2 2023 \
 Example output: the date is even number: 0

 Example input: / 10 3 2023 \
Example output: the date is even number: 1

##### Task #2:

2.1 Define an `enum` with constants for each month \
2.2 Print the values on the standard output as defined in section 1.2 but instead of a numerical value for the month, print it as a word; **Hint: use `switch` operator** 

Example input: . 1 2 2023 \
Example output: 01.February.2023

2.3 Print on the standard output how many days the month has; **Hint use `if-else if-else` statement**

Example input: . 1 2 2023 \
Example output: The month has 28 days

2.4 Write a `for` loop to print all the dates till the end of the month, each on a new line \
\
Example input: . 25 2 2023 \
Example output:  \
Upcoming detes: \
26.02.2023 \
27.02.2023 \
28.02.2023

2.5 Write a `while` or `do-while` loop to print all the dates from the beginning of the month, each on a new line \
\
Example input: . 4 2 2023 \
Example output:  
Passed detes: \
03.02.2023 \
02.02.2023 \
01.02.2023

2.6 Let 1st of July be the middle of a year and beginning of its second half; Print all the remaining days up to: 
* 1st of July if the month entered is in the first half of the year
* 31st of December if the month entered is in the second half of the year

Print each date on a new line; The month is printed as numerical value; **Hint: use nested loops**

##### Task #3:

3.1 Add a new header file to your project called "date_time_types.h" and: 
* Place the `enum` from Task#2.1 into it 
* Compile the project
* From now on place the custom types into it

3.2 Add a new header file called "date_time_functions.h"; From now on, place declarations in it for all the functions 
that you write

3.3 Add a new source file called "date_time_functions.c"; From now on, place definitions in it for all the functions
that you write

3.4 C does not have a boolean type; Add an `enum` called 'Boolean' with values 'FALSE=0' and 'TRUE=1'

3.5 Move to a function the implmentaion of Task#1.2

3.6 Write a function that checks if a given year is a leap year

3.7 Write a function that returns the number of days in a given month

3.8 Write a function that adds a given number of days to a date; The function has three
parameters - day, month, year; The number of days to add is read from the standard input; Print the resulting date on the standard output by calling the function from Task#3.5

3.9 Write a function that subtracts a given number of days from a date; The function has three
parameters - day, month, year; The number of days to subtract is read from the standard input; Print the resulting date on the standard output by calling the function from Task#3.5

3.10 Write a function that increments a given date; Print the resulting date on the standard output by calling the function from Task#3.5

3.11 Write a function that decrements a given date; Print the resulting date on the standard output by calling the function from Task#3.5

3.12 In the `main` function, **in a loop**: 
* Perform the same steps as in Task#1.1
* Skip the current iteration if the same date as is in the previous iteration is read 
* Break out of the loop if the date, month and year are equal to -1
* Print information about the date in the following format: \
\
Example input: . 28 6 2023 \
 Example output: \
 28.06.2023 \
 2023 is leap year: false \
 June has 30 days \
 The next date is 29.06.2023 \
 The previous date is 27.06.2023

* Read two values from the standard input - operation symbol and number of days 
* If the operation symbol is '+', add the number of days to the date \
\
Example input: + 5 \
 Example output: 
 28.06.2023 + 5 days = 03.07.2023
 
* If the operation symbol is '-' subtract the number of days from the date \
\
Example input: - 5 \
 Example output: 
 28.06.2023 - 5 days = 23.06.2023
 
##### Task #4:

4.1 In the `main` function, define an array with the following values denoting timezone offsets: [1, 5, -4, 3, -3, 2]

4.1.1 Write a function that: 
* Prints the array on the standard output 
* Accesses the elements via index using `size_t` for the type of the index variable
* Prints all elements on a single line, separated by comma

4.1.2 Wtite a function that: 
* Accesses the elements of the array via a pointer starting from the first element
* Increments each element in the array **which is positive value**
* Decrements each element in the array **which is negative value**

4.1.3 Write a function that: 
* Accesses the elements of the array via a pointer **starting from the last element**
* Multiplies each value by its **corresponding index**

4.2 Write a function that prints on the standard output the addresseses of the frist, middle and last elements
\
Example output:\
The first elements is at address 0x...; The middle element is at address 0x...; The last element is at address 0x...

4.3 Write a function with **two pointer parameters** that can print the timezone array both forward and backward

4.4 Write a function that:  
* Finds the indexes of the smallest and biggest offset(element) from the array
* Stores the indexes in a pointer parameters of type `size_t`

Example output:\
The smallest timezone offset(-4) is with index 2 at address 0x...\
The largest timezone offset(5) is with index 1 at address 0x...\
\

4.6 Write a function that accepts two addresses as parameters and swaps the values that they point to; The pointers can point to values in the timezone offsets array; It also prints on the standard output information about the operation in the following format:\
\
Example output:\
Before swap: value ... at address 0x...; value ... at address 0x...\
After swap: value ... at address 0x...; value ... at address 0x...

4.7 Write a function that reads two values from the standard input, an index in the array and a value - new timezone offset;
Use the function from Task#4.6 to update the value in the array  

4.8 Write a function that sorts the array in descending order

#### Task #5

5.0 Requirements for a timezone abbreviations: 
* Consists only of capital letters
* Is not longer than 5 symbols

5.1 Use the following definition of an array with timezone abbreviations:
`char* timezone_abbreviations[] = {"ADT", "AST", "CAT", "CCT", "GALT", "HKT", "NDT", "NZST", "TOST", "YAKT"}`

5.2 Define a similar array called `timezone_full_names` with the full names matching the abbreviations above: 
* Atlantic Daylight Time
* Arabia Standard Time
* Central Africa Time
* Cocos Islands Time
* Galapagos Time
* Hong Kong Time
* Newfoundland Daylight Time
* New Zealand Standard Time
* Tonga Summer Time
* Yakutsk Time

5.3 Write a function that returns the full timezone name by a given abbreviation. More details:
* The abbreviation must be passed to the function as a `char*`
* The function looks for the given abbreviation in the `timezone_abbreviations`
* If the abbreviation is found, the full name of the timezone is at the same position in the `timezone_full_names` array
* The function returns a `char*` - pointing either to an element into `timezone_full_names` (if the lookup is successful) or a suitable invalid value (if the lookup is not successful)

5.4 In the `main` function, **in an endless loop**, perform the following steps: 
* Read a string from the standard input
* If the string equals "exit" - break out of the loop 
* If the string meets the requirements from Task#5.0 call the function from Task#5.3 and pass the user input as argument; **Note that the user is allowed to enter lower case letters, the program must convert them to upper case before searching for the abbreviation**
* If the function succeeds, it prints on the standard output information about the timezone in the following format: "The abbreviation ... is ..."
* If the function fails, it prints that this abbreviation is unknown

Additional guidelines: 
* Use the `str*` library functions for comparing strings, calculating string lengths, etc
* Use the library functions `isupper`, `islower`, `toupper`, `tolower` to handle lower/upper case letters
* Write separate functions for subtasks like converting between lower/upper case, etc. 

## Coding guidelines
* Use "snake case" for variable and function names (e.g `foo_value` instead of `fooValue`)
* Do NOT start your variable names with '_'
* Do NOT name your variables with a single letter (e.g 'd' intstead of 'date', 'm' isntead of 'month', etc)
* Avoid magic numbers (use `const`ants, `enum`arators, or `#define`s)
* Use lower case for file names

## Commit message rules
Each commit message must be in the format: *"Task#X: Descriptive information what has changed"*, where X is the task number

Example commit messages for changes related to Task#1:
* *"Task#1: Added reading of date, month, year"*
* *"Task#1: Correction of the print format"*
