#include <stdio.h>
#include <string.h>
#include <ctype.h>


#define abbr_length 5 //Is not longer than 5 symbols
/*#define num_timezone 10*/
char* timezone_abbreviations[] = {"ADT", "AST", "CAT", "CCT", "GALT", "HKT", "NDT", "NZST", "TOST", "YAKT"};
char* timezone_full_names[] = {"Atlantic Daylight Time", "Arabia Standard Time", "Central Africa Time", "Cocos Islands Time", "Galapagos Time", "Hong Kong Time", "Newfoundland Daylight Time", "New Zealand Standard Time", "Tonga Summer Time", "Yakutsk Time"};

char* get_timezone_name(char* abbreviation) // vzima abbreviation kato input i tursi abbreviation v timezone_abbreviations (taka raboti sigorno moje i po lesno da stane)
{
    int i;
    for (i = 0; i < 10 ; i++) // moje da se definiram num_timezone
    {
        if (strcmp(abbreviation, timezone_abbreviations[i]) == 0)
        {
            return timezone_full_names[i];  // vrushta cqloto ime
        }
    }
    return "unknown"; // unkniwn ako ne nameri full_name
}

int main()
{
    char input[abbr_length]; // moje da smenqme duljinata
    while (1)
    {
        printf("Enter a timezone abbreviation (or 'exit' to quit): ");
        scanf("%s", input);
        if (strcmp(input, "exit") == 0)
        {
            break; // spira loopa s input exit
        }
        int i;
        for (i = 0; i < strlen(input); i++) // pravi bukvite na glavni
        {
            input[i] = toupper(input[i]);
        }
        if (strlen(input) <= abbr_length) // proverqva dali duljinata e tochna za da moje da produlji s pravilen otgovo ili "Unknown timezone abbreviation"
        {
            char* timezone_name = get_timezone_name(input);
            if (strcmp(timezone_name, "unknown") == 0)
                {
                printf("Unknown timezone abbreviation\n");
                }
            else
                {
                printf("The abbreviation %s is %s\n", input, timezone_name);
                }
        }
        else
            {
            printf("Invalid timezone abbreviation\n"); // ako ne e v limita input>5
            }
    }
    return 0;
}
